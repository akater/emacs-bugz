# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: bugz-section
#+subtitle: Representing bugz (and collections thereof) in buffers with ORPB
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle bugz-section.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

This requires orpb so this feature is separated.

* Dependencies
#+begin_src elisp :results none
(eval-when-compile (require 'bugz-macs))
(require 'orpb)
(require 'bugz-core)
(require 'bugz-core)                         ; bugz-get, bugz-show, bugz-search
#+end_src

* Class ~with-description-markers~
#+begin_src elisp :results none
(defclass bugz-section-with-description-markers (orpb-section)
  ;; this does not inherit from bugz-section, bad naming
  ((description-start :type (or null marker)
                      :initform (make-marker))
   (description-end :type (or null marker)
                    :initform (make-marker)))
  (:documentation "This class is mostly needed for cleaner exposition and maybe better refined slot type specifications in bugz-bug-representable and bugz-section-item, when refined slot type specifications are supported in EIEIO."))
#+end_src

* Class ~bug-representable~
** TODO Maybe rename to representable-reactively
- State "TODO"       from              [2022-12-17 Sat 07:34]
** Definition
Elements of ~orpb-sections~ list must indeed be of the type mentioned, including section-with-description-markers; due to [[Method: setf description of a representable bug]].
#+begin_src elisp :results none
(defclass bugz-bug-representable (orpb-representable-entity bugz-bug)
  (
   ;; Not yet, Elisp fails at type refinements for now:
   ;; (orpb-sections
   ;;  :type (list-of (and orpb-reactive
   ;;                      bugz-section-with-description-markers)))
   ))
#+end_src

* insert-truncated-string
**** insert-truncated-string
***** Prerequisites
****** ellipsis
#+begin_src elisp :results none
(defconst bugz--horizontal-ellipsis
  (char-from-name "HORIZONTAL ELLIPSIS"))
#+end_src

****** insert-truncated-string
******* TODO Maybe use maxlength instead of start, end
- State "TODO"       from              [2022-12-20 Tue 00:45]
******* Definition
#+begin_src elisp :results none
(defun bugz--insert-truncated-string (string &optional start end
                                             single-char-ellipsis)
  "Insert string at point so that it fits between start and end

The point is, ensure no line wrap which means start is
current-column, end is window-width.  But sometimes start and end
may be different."
  (setf start (or start (point-min))
        end (or end (point-max)))
  (let ((chars-left (- end start 1)))
    (if (<= (length string) chars-left)
        (insert string)
      (cl-loop for i below (- chars-left (if single-char-ellipsis 1 3))
               do (insert (aref string i))
               finally (insert (if single-char-ellipsis
                                   bugz--horizontal-ellipsis
                                 "..."))))))
#+end_src

* insert-truncated-description
#+begin_src elisp :results none
(cl-defun bugz-section--insert-truncated-description ( string
                                                       start-marker end-marker
                                                       &key start end
                                                       check-line-end)
  "Insert at point, set markers, return STRING."
  (when check-line-end (cl-assert (looking-at-p (rx line-end))))
  (set-marker start-marker (point))
  (prog1 (bugz--insert-truncated-string string start end)
    (set-marker end-marker (point))))
#+end_src

* Method: setf description of a representable bug
** Definition
#+begin_src elisp :results none
(cl-defmethod (setf bugz-bug-description) :after (new-value
                                                  (b bugz-bug-representable))
  "Whenever description of a bug is updated, update it in its representations."
  (dolist (section (orpb-sections b))
    (with-slots (description-start description-end) section
      (if (null (marker-buffer description-start))
          (warn
           ;; We should not check whether buffer exists.
           ;; Buffers should be deleted from these lists when they are killed.
           "Invalid marker in section object %s; this should not happen"
           section)
        (with-current-buffer (marker-buffer description-start)
          (save-excursion
            (let ((inhibit-read-only t))
              (goto-char description-start)
              ;; Preserving properties:
              (let ((keymap (get-text-property (point) 'keymap))
                    ;; (orpb-section (get-text-property 'orpb-section (point)))
                    )
                ;; For the time being, we track two hardcoded ones
                ;; and presume they are the same across the region.
                (delete-region description-start description-end)
                (prog1 (bugz-section--insert-truncated-description
                        new-value description-start description-end
                        :start (current-column) :end (window-width))
                  (put-text-property description-start (point)
                                     'orpb-section section)
                  (put-text-property description-start (point)
                                     'keymap keymap))))))))))
#+end_src

* Class ~section~
** Definition
#+begin_src elisp :results none
(cl-defclass bugz-section (orpb-section-with-counter)
  ()
  (:documentation "Section for various lists of bugs"))
#+end_src

** Tests
*** Precedence list
#+begin_src elisp :tangle no :results code :wrap example elisp
(class-precedence-list/names 'bugz-section)
#+end_src

#+RESULTS:
#+begin_example elisp
(bugz-section orpb-section-with-counter orpb-section-with-heading hie-parent-component orpb-section hie-component eieio-default-superclass)
#+end_example

#+EXPECTED:
#+begin_example elisp
(bugz-section orpb-section-with-counter orpb-section-with-heading orpb-section hie-parent-component hie-component eieio-default-superclass)
#+end_example

* item-map
** Definition
#+begin_src elisp :results none
(defvar bugz-section-item-map
  (let ((map (make-sparse-keymap)))
    ;; (define-key map [remap magentoo-visit-thing] 'bugz-section-show-bug)
    (define-key map (kbd "RET") 'bugz-show)
    ;; a for apply
    ;; (define-key map "a" 'bugz-section-apply-changes)
    ;; (define-key map "k" 'bugz-section-item-drop)
    map)
  "Keymap for individual `bugz' items.")
#+end_src

* Class ~item~
** Emacs 30 (?) and later
:PROPERTIES:
:header-args:    :tangle (if (>= emacs-major-version 30) "bugz-section.el" "")
:END:
I was certain the fix was merged into Emacs but the behavior is still wrong.
#+begin_src elisp :results none
(defclass bugz-section-item (orpb-reactive
                             orpb-child-section
                             bugz-section-with-description-markers)
  ((keymap :initform bugz-section-item-map)
   ;; Not yet, Elisp fails at type refinements for now:
   ;; (value :type bugz-bug-representable)
   ;; child-section does not yet have a parent slot
   ;; (parent :type bugz-section)
   )
  :documentation "Section representing an individual Bugzilla bug")
#+end_src

** Emacs 27 and earlier
:PROPERTIES:
:header-args:    :tangle (if (< emacs-major-version 30) "bugz-section.el" "")
:END:
#+begin_src elisp :results none
(defclass bugz-section-item (orpb-reactive
                             orpb-child-section
                             bugz-section-with-description-markers)
  ((keymap :initform (eval `bugz-section-item-map t))
   ;; Not yet, Elisp fails at type refinements for now:
   ;; (value :type bugz-bug-representable)
   ;; child-section does not yet have a parent slot
   ;; (parent :type bugz-section)
   )
  :documentation "Section representing an individual Bugzilla bug")
#+end_src

* fetch-bug
This might not be needed
#+begin_src elisp :tangle no :results none
(defun bugz-section-fetch-bug (section)
  "Call bugz get for bug at point

SECTION must be bugz-section-item."
  (interactive (list (get-text-property (point) 'orpb-section)))
  (cl-etypecase section
    ;; (bugz-section-last-comment
    ;;  ;; likely, we should do the same we do at bugz-item
    ;;  )
    (bugz-section-item
     (let ((bug (oref section value)))
       (bugz-get (oref bug bug-tracker) (oref bug bugid))))))
#+end_src

* show-bug
This might not be needed
#+begin_src elisp :results none
(defun bugz-section-show-bug (section)
  "Show bug at point

SECTION must be bugz-section-item."
  (interactive (list (get-text-property (point) 'orpb-section)))
  (cl-etypecase section
    ;; (bugz-section-last-comment
    ;;  ;; likely, we should do the same we do at bugz-item
    ;;  )
    (bugz-section-item
     (bugz-show (oref section value)))))
#+end_src

* Insert section for a standalone bug
#+begin_src elisp :results none
(cl-defmethod orpb-insert-section ( (s bugz-section-item)
                                    (b
                                     ;; does not seem to have to be
                                     ;; bugz-bug-representable
                                     bugz-bug))
  "Insert a standlone Bugzilla bug."
  ;; By this moment, b should be eq to (oref s value)
  ;; TODO: Maybe write a :before method to check this.
  (with-slots (description-start description-end) s
    (let ((start (point)))
      (prin1 (bugz-bugid b) (current-buffer))
      (put-text-property start (point)
                         'face
                         ;; this did not work
                         'orpb-section-highlight)
      ;; this doesn't work
      ;; (font-lock-ensure start (point))
      )
    (insert ?\s)
    ;; Depending on context, we might want to insert bug-tracker too.
    ;; However, this will happen rarely, if ever,
    ;; so we do not implement it for now
    (bugz-section--insert-truncated-description
     (bugz-bug-description b) description-start description-end
     :start (current-column) :end (window-width)
     :check-line-end t)
    (insert ?\n)))
#+end_src

* insert-section
** Definition
#+begin_src elisp :results none
(defun bugz-insert-section (bug)
  "BUG should be a bugz bug."
  (cl-check-type bug bugz-bug)
  (orpb-insert-section 'bugz-section-item bug))
#+end_src

* TODO define-bugz-query
- State "TODO"       from              [2022-12-30 Fri 18:55] \\
  Idea: expand to a bunch of forms, incl. defclass bugz-section-myquery, like defclass bugz-section-reported-by-me.  Have value initformed to the unique object automatically updated whenever bugz-bugs is.
* insert-bugz-reported-by-me
** Prerequisites
*** Class ~reported-by-me~
#+begin_src elisp :results none
(cl-defclass bugz-section-reported-by-me (bugz-section)
  (
   ;; (value :allocation :class :initform bugz-reported-by-me-object)
   ))
#+end_src

** Definition
#+begin_src elisp :results none
(cl-defmethod orpb-insert-heading ((s bugz-section-reported-by-me)
                                   x)
  (ignore s x)
  (insert "Bugs reported by me"))
#+end_src

#+begin_src elisp :results none
(cl-defun bugz-insert-bugz-reported-by-me ( &optional
                                            (bug-tracker
                                             bugz-default-instance)
                                            insert-empty)
  (let ((section (make-instance 'bugz-section-reported-by-me)))
    (setf (oref section children)
          ;; todo: provide an option to always use cache here.
          (mapcar
           ;; could be cl-map-into but not when bugz-is-my-only-poster
           (lambda (bug) (make-instance 'bugz-section-item
                                        :value bug :parent section))
           (let ((default-directory
                   ;; we want user's settings for bugz,
                   ;; not admin's
                   ;; --- relevant for magentoo buffers
                   "~"))
             (if bugz-is-my-only-poster
                 ;; There's a problem when bug-tracker is nil.
                 ;; It's best to suggest users to set bugz-default-instance
                 ;; if they want to use this option.
                 (with-memoization (alist-get bug-tracker bugz-reported-by-me
                                              nil nil
                                              #'bugz-bug-tracker-equal)
                   ;; this is overall wrong:
                   ;; indexes such as reported-by-me
                   ;; ore updated on when bug objects are instantiated
                   (bugz-search bug-tracker ()
                                :creator bugz-user-mail-address))
               (bugz-search bug-tracker ()
                            :creator bugz-user-mail-address))
             ;; todo: It'd be good if we could get last-updated time
             ;; with bugz search
             )))
    (when (or (oref section children) insert-empty)
      (progn
        ;; unwind-protect
        (orpb-operate hie-insert-marked section)
        ;; we don't call operate propertize in an after method for operate insert
        ;; because then we can't avoied excessive applications of methods
        ;; to children:
        ;; - kids' properties get applied recursively,
        ;; - parent's properties get applied,
        ;; - kids' properties get applied recursively again.
        ;; We don't want the first part to happen as it's redundant.
        ;; So this is how it goes so far.
        ;; Theoretically, applying parent's properties over kids'
        ;; could have some merit
        ;; but unlikely so.
        (orpb-operate hie-propertize section)))
    section))
#+end_src

* Other classes
#+begin_src elisp :results none
(cl-defclass bugz-section-assigned-to-me (bugz-section)
  (
   ;; (value :allocation :class :initform bugz-assigned-to-me-object)
   ))
#+end_src

#+begin_src elisp :results none
(cl-defmethod orpb-insert-heading ((s bugz-section-assigned-to-me)
                                   x)
  (ignore s x)
  (insert "Bugs assigned to me"))
#+end_src

#+begin_src elisp :results none
(cl-defclass bugz-section-other-bugs (bugz-section)
  (
   ;; (value :allocation :class :initform bugz-other-bugs-object)
   ))
#+end_src

#+begin_src elisp :results none
(cl-defmethod orpb-insert-heading ((s bugz-section-other-bugs)
                                   x)
  (ignore s x)
  (insert "Other bugs"))
#+end_src

* insert-bugz
** Prerequisites
*** make-section
**** Definition
#+begin_src elisp :results none
(defun bugz--make-and-insert-section ( class-name list-of-bugs
                                       &optional insert-empty insert-after)
  (let ((section (make-instance class-name)))
    (setf (oref section children)
          ;; todo: provide an option to always use cache here.
          (mapcar
           ;; could be cl-map-into but not when bugz-is-my-only-poster
           (lambda (bug) (make-instance 'bugz-section-item
                                        :value bug :parent section))
           list-of-bugs))
    (when (or (oref section children) insert-empty)
      (progn
        ;; unwind-protect
        (orpb-operate hie-insert-marked section)
        ;; we don't call operate propertize in an after method for operate insert
        ;; because then we can't avoied excessive applications of methods
        ;; to children:
        ;; - kids' properties get applied recursively,
        ;; - parent's properties get applied,
        ;; - kids' properties get applied recursively again.
        ;; We don't want the first part to happen as it's redundant.
        ;; So this is how it goes so far.
        ;; Theoretically, applying parent's properties over kids'
        ;; could have some merit
        ;; but unlikely so.
        (save-excursion (orpb-operate hie-propertize section))
        (when insert-after (insert insert-after))))
    section))
#+end_src

** Definition
#+begin_src elisp :results none
(cl-defun bugz-insert-bugs ( &optional (bug-tracker bugz-default-instance)
                             insert-empty)
  (bugz-sync-indexes bug-tracker)
  (let ((assigned-to-me (alist-get bug-tracker bugz-assigned-to-me
                                   nil nil
                                   #'bugz-bug-tracker-equal))
        (reported-by-me (alist-get bug-tracker bugz-reported-by-me
                                   nil nil
                                   #'bugz-bug-tracker-equal)))
    (bugz--make-and-insert-section 'bugz-section-assigned-to-me
                                   assigned-to-me
                                   insert-empty ?\n)
    (bugz--make-and-insert-section 'bugz-section-reported-by-me
                                   reported-by-me
                                   insert-empty ?\n)
    (bugz--make-and-insert-section
     'bugz-section-other-bugs
     (cl-loop for bug in bugz-bugs
              when (bugz-bug-tracker-equal bug-tracker
                                           (oref bug bug-tracker))
              unless (or (memq bug assigned-to-me)
                         (memq bug reported-by-me))
              collect bug)
     insert-empty)))
#+end_src
