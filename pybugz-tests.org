# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: pybugz-tests
#+subtitle: Tests for pybugz
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle pybugz-tests.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

* Dependencies
#+begin_src elisp :results none
(eval-when-compile (require 'cl-lib))
(require 'ort)
#+end_src

* Installing and setting up Bugzilla
A test instance of Bugzilla is needed for tests.  We can't configure it on our own, unfortunately.

The following explains the Gentoo setup.  Hopefully it'll be useful to users of other systems.  Explanations of other setups are welcome; however, we should keep them all consistent to ensure test environment is always the same.

We presume =apache2= web server.

** Installation
Emerging =app-emacs/pybugz= with =test= USE flag should install Bugzilla.  We briefly explain what it does:

Set apache2 modules in =make.conf=:
#+begin_example conf-unix
# The minimal set of modules supported:
APACHE2_MODULES="authn_core authz_core authz_host dir mime unixd socache_shmcb"
# and cgi for testing elisp-pybugz:
APACHE2_MODULES="${APACHE2_MODULES} cgi"
#+end_example

USE flags:
#+begin_example conf-space
www-apps/bugzilla apache2 sqlite
#+end_example

Install Bugzilla:
#+begin_example sh
emerge www-apps/bugzilla
#+end_example

Python's bugz works via xmlrpc so install optfeatures to enable it:
#+begin_example sh
emerge dev-perl/SOAP-Lite dev-perl/XMLRPC-Lite dev-perl/Test-Taint
#+end_example

Note: Gentoo mentions all three with respect to xmlrpc interface, not just =dev-perl/XMLRPC-Lite=, which is why I mention all three here.  Bugzilla 5.0 itself only mentions =dev-perl/XMLRPC-Lite=.

** Configuration
At =/sudo::/var/www/localhost/htdocs/bugzilla= evaluate
#+begin_example sh
./checksetup.pl
#+end_example

This will perform initial setup.  Find =localconfig= in the same directory and set ~$db_driver~ to ~'sqlite'~.  Then, create admin with the same command:
#+begin_example sh
./checksetup.pl
#+end_example

enter the admin user name: =elisp-pybugz@localhost.localdomain=
enter the password: =elisp-pybugz=

To =/sudo::/etc/apache2/vhosts.d/default_vhost.include= , add
#+begin_example conf-space
<Directory "/var/www/localhost/htdocs/bugzilla">
  AddHandler cgi-script .cgi
  Options +ExecCGI +FollowSymLinks
  DirectoryIndex index.cgi index.html
  AllowOverride All
</Directory>
#+end_example

and save this file.

#+begin_example sh
/etc/init.d/apache2 restart
./testserver.pl http://localhost/bugzilla
#+end_example

All tests should pass.

** TODO Wipe the database at the start of each test session
- State "TODO"       from              [2022-11-24 Thu 08:39]
  On Gentoo, we could also try to copy =/var/www/localhost/htdocs/bugzilla/= but it's only readable from root.
* Type ~list-of~
** Dependencies
#+begin_src elisp :results none
(require 'bytecomp)
#+end_src

** Prerequisites
*** proper-list-with-elements-of-type-p
**** Examples
***** Basic Examples
****** TEST-PASSED Check all list elements for given type
#+begin_src elisp :tangle no :results code :wrap example elisp
(pybugz-proper-list-with-elements-of-type-p 'symbol '(a b c))
#+end_src

#+EXPECTED:
#+begin_example elisp
t
#+end_example

***** Scope
****** TEST-PASSED Check fails on improper lists
#+begin_src elisp :tangle no :results code :wrap example elisp
(pybugz-proper-list-with-elements-of-type-p 'symbol '(a . b))
#+end_src

#+EXPECTED:
#+begin_example elisp
nil
#+end_example

***** Properties and Relations
****** TEST-PASSED All elements of empty list are of any type
#+begin_src elisp :tangle no :results code :wrap example elisp
(pybugz-proper-list-with-elements-of-type-p 'symbol nil)
#+end_src

#+EXPECTED:
#+begin_example elisp
t
#+end_example

**** Definition
#+begin_src elisp :results none
(defun pybugz-proper-list-with-elements-of-type-p (type list)
  (let ((result t))
    (while list
      (unless (and (consp list)
                   (cl-typep (pop list) type))
        ;; pnullf not defined yet, bad
        (cl-psetf list nil
                  result nil)))
    result))
#+end_src

**** Tests
***** TEST-PASSED Singleton list
#+begin_src elisp :tangle no :results code :wrap example elisp
(pybugz-proper-list-with-elements-of-type-p 'symbol '(a))
#+end_src

#+EXPECTED:
#+begin_example elisp
t
#+end_example

** Definition
#+begin_src elisp :results none
(unless (get 'list-of 'cl-deftype-handler)
  ;; Do not redefine the type if defined already
  (defvar pybugz--type-checkers/list-of nil)
  (cl-deftype list-of (type)
    `(and list
          (satisfies
           ,(with-memoization (alist-get type pybugz--type-checkers/list-of
                                         nil nil #'equal)
              (let ((list (let ((gensym-counter 0)) (gensym "list-"))))
                (byte-compile
                 `(lambda (,list)
                    ,(format "Check %s to be a (proper) list and check all its elements for type %s"
                             (upcase (symbol-name list)) type)
                    (pybugz-proper-list-with-elements-of-type-p
                     ',type ,list)))))))))
#+end_src

* list-of-strings-p
#+begin_src elisp :results none
(defun list-of-strings-p (x)
  ;; This won't compile:
  ;; (cl-typep x `(list-of string))
  (and (listp x) (cl-every #'stringp x)))
#+end_src

* python-traceback-p
#+begin_src elisp :results none
(defun pybugz--python-traceback-p (x)
  (string-prefix-p "Traceback" x))
#+end_src

* credentials
In pybugz form,
#+begin_example conf-unix
user = elisp-pybugz@localhost.localdomain
password = elisp-pybugz
#+end_example

* (likely won't work) run
** Definition
#+begin_src elisp :results none
(defun pybugz-tests-run (&optional directory-with-code)
  "Test `pybugz' package.

Should be called in directory with org files for `pybugz' package.

Code (compiled or not) is presumed to be contained in
DIRECTORY-WITH-CODE which defaults to “.”.  Relative names are
resolved with respect to `default-directory'.

If code is not found in DIRECTORY-WITH-CODE, it is looked up
in (the rest of) `load-path'."
  (setq directory-with-code
        (file-name-as-directory
         (expand-file-name (or directory-with-code "."))))
  (let ((local-load-path (cons directory-with-code load-path)))
    (let (org-confirm-babel-evaluate)

      ;; pybugz

      ;; Code to be tested:
      (let ((load-path local-load-path))
        (load "pybugz"))

      ;; Org file with tests:
      (ort-run-tests-in-file "./pybugz.org" :save-buffer t))))
#+end_src
