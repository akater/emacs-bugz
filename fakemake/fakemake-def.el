;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'bugz
  authors "Dima Akater"
  first-publication-year-as-string "2022"
  org-files-in-order '("bugzilla"
                       "pybugz-interface"
                       "pybugz-definitions"
                       "pybugz"
                       "bugz-macs"
                       "bugz-bugzpost"
                       "bugz-core"
                       "bugz-backend-pybugz"
                       "orpb"
                       "bugz-section"
                       "bugz")
  org-files-for-testing-in-order '("bugz-tests")
  site-lisp-config-prefix "50"
  license "GPL-3")
