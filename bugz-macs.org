# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: bugz-macs
#+subtitle: Miscellanous macros and inlined functions for the bugz package
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle bugz-macs.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

* cl-defclass
** Dependencies
#+begin_src elisp :results none
(eval-when-compile (require 'mmxx-macros-defmacro))
(require 'cl-lib)
(require 'eieio)
#+end_src

** Prerequisites
*** macro-function
**** Examples
***** Basic Examples
****** TEST-PASSED Return the macroexpanding function
#+begin_src elisp :tangle no :results code :wrap example elisp :success-predicate #'functionp
(bugz--macro-function '1value)
#+end_src

#+EXPECTED:
#+begin_example elisp
#[257 "\207"
      []
      2 2695554]
#+end_example

****** TEST-PASSED No (global) macro
#+begin_src elisp :tangle no :results code :wrap example elisp
(fmakunbound 'bugz--test-nonexistent-macro)
(bugz--macro-function 'bugz--test-nonexistent-macro)
#+end_src

#+EXPECTED:
#+begin_example elisp
nil
#+end_example

***** Scope
****** TEST-PASSED A function but not a macro
#+begin_src elisp :tangle no :results code :wrap example elisp
(bugz--macro-function 'consp)
#+end_src

#+EXPECTED:
#+begin_example elisp
nil
#+end_example

****** TEST-PASSED A special form
#+begin_src elisp :tangle no :results code :wrap example elisp
(bugz--macro-function 'if)
#+end_src

#+EXPECTED:
#+begin_example elisp
nil
#+end_example

**** Definition
#+begin_src elisp :results none
(eval-and-compile
  (defun bugz--macro-function (symbol &optional environment)
    "Return the function that expands the macro SYMBOL in the global environment.

Return nil if SYMBOL is not the name of a macro.

Differs from CL in that the returned function does not accept two arguments
(the whole form and environment).  Hence, this one is not named “cl-macro-function”."
    (ignore environment)
    ;; maybe return
    ;; (assq symbol macroexpand-all-environment)
    ;; when environment is supplied
    (cl-flet ((destructuring-error ()
                (error "Can't destructure the value of %s"
                       `(symbol-function ',symbol))))
      (let ((symbol-function (symbol-function symbol)))
        (cl-typecase symbol-function
          (cons
           (cl-destructuring-bind (macro . f) symbol-function
             (unless (and (eq 'macro macro)
                          (functionp f))
               (destructuring-error))
             f))
          ((or null function subr))
          (t (destructuring-error)))))))
#+end_src

**** Tests
***** TEST-PASSED nil
#+begin_src elisp :tangle no :results code :wrap example elisp
(bugz--macro-function nil)
#+end_src

#+EXPECTED:
#+begin_example elisp
nil
#+end_example

***** TEST-PASSED t
#+begin_src elisp :tangle no :results code :wrap example elisp
(bugz--macro-function t)
#+end_src

#+EXPECTED:
#+begin_example elisp
nil
#+end_example

***** TEST-PASSED A built-in macro
#+begin_src elisp :tangle no :results code :wrap example elisp
(functionp (bugz--macro-function 'push))
#+end_src

#+EXPECTED:
#+begin_example elisp
t
#+end_example

***** TEST-PASSED not a symbol
#+begin_src elisp :tangle no :results code :wrap example elisp
(bugz--macro-function 0)
#+end_src

#+EXPECTED:
#+begin_example elisp
(wrong-type-argument symbolp 0)
#+end_example

*** setf plist-get
#+begin_src elisp :results none
(when (< emacs-major-version 28)
  (require 'macroexp)
  (require 'gv)
  (gv-define-expander plist-get
    (lambda (do plist prop)
      (macroexp-let2 macroexp-copyable-p key prop
        (gv-letplace (getter setter) plist
          (macroexp-let2 nil p `(cdr (plist-member ,getter ,key))
            (funcall do
                     `(car ,p)
                     (lambda (val)
                       `(if ,p
                            (setcar ,p ,val)
                          ,(funcall setter `(cons ,key (cons ,val ,getter))))))))))))
#+end_src

** Definition
#+begin_src elisp :results none
(defmacro-mmxx cl-defclass (class-name &rest stuff
                                       &gensym name rest)
  "Not really cl-compatible but resolves some issues in this elisp package."
  (declare (doc-string 4))
  (let ((macro-function (bugz--macro-function 'defun)))
    `(cl-macrolet ((defun (,name &rest ,rest)
                     (unless (eq ,name ',class-name)
                       (apply ,macro-function ,name ,rest))))
       (prog1 (defclass ,class-name ,@stuff)
         (setf (plist-get (eieio--class-options
                           (cl-find-class ',class-name))
                          :method-invocation-order)
               :c3)))))
#+end_src

* delqf
** Dependencies
#+begin_src elisp :results none
(require 'cl-lib)
(require 'macroexp)
#+end_src

** Prerequisites
*** check-delqf-count
#+begin_src elisp :results none
(eval-when-compile
  (defsubst bugz--check-delqf-count (value)
    (unless (natnump value)
      (error "%s count parameter is %s but it should be nil or a non-negative integer"
             'delqf value))))
#+end_src

*** make-delqf-code
**** Prerequisites
***** macroexp-or
#+begin_src elisp :results none
(defun macroexp-or (exps)
  "Return EXPS (a list of expressions) with `or' prepended.
If EXPS is a list with a single expression, `or' is not
prepended, but that expression is returned instead."
  (if (cdr exps) `(or ,@exps) (car exps)))
#+end_src

**** Definition
#+begin_src elisp :results none
(eval-when-compile
  (defun bugz--make-delqf-code-with-non-null-count (item place count)
    "If COUNT is nil, presume :count is 1."
    (let ((item-g (make-symbol "item"))
          (list (make-symbol "list"))
          (left-to-delq (when count (make-symbol "left-to-delq"))))
      (let ((maybe-decf-counter (when count `((cl-decf ,left-to-delq)))))
        `(let ((,item-g ,item) (,list ,place)
               ,@(when count `((,left-to-delq ,count))))
           ,@(cl-psetf item item-g)
           ,@(when count
               `((bugz--check-delqf-count ,left-to-delq)))
           (cond
            (,(macroexp-or
               `((null ,list) ,@(when count `((zerop ,left-to-delq))))))
            ,@(unless count
                `(((eq ,item (car ,list))
                   (pop ,place)
                   (setf ,list ,place))))
            (t
             ,@(when count
                 `((when (eq ,item (car ,list))
                     (pop ,place)
                     (setf ,list ,place)
                     ,@maybe-decf-counter)))
             ,(wrap-if count `(unless (zerop ,left-to-delq) ,form)
                form
                `(cl-block nil
                   (let ((tail ,list))
                     (while (cdr tail)
                       (when (eq ,item (cadr tail))
                         (pop (cdr tail))
                         ,@maybe-decf-counter
                         ,(wrap-if count `(when (zerop ,left-to-delq) ,form)
                            form `(cl-return)))
                       (pop tail)))))))
           ,list)))))
#+end_src

** Definition
#+begin_src elisp :results none
(cl-defmacro delqf (item place &key count &aux zero-count-p)
  (cond
   ((null count) `(setf ,place (delq ,item ,place)))
   ((and (macroexp-const-p count)
         (let ((n (eval count t)))
           (and n
                (progn
                  (bugz--check-delqf-count n)
                  (or (setq zero-count-p (= 0 n))
                      (= 1 n))))))
    (if zero-count-p
        place
      (bugz--make-delqf-code-with-non-null-count item place nil)))
   (t
    (bugz--make-delqf-code-with-non-null-count item place count))))
#+end_src

** Tests
*** TEST-PASSED item place count 1
#+begin_src elisp :tangle no :results code :wrap example elisp :keep-expected t
(macroexpand-1 `(delqf item place :count 1))
#+end_src

#+EXPECTED:
#+begin_example elisp
(let ((item item)
      (list place))
  (cond
   ((null list))
   ((eq item (car list))
    (pop list))
   (t
    (cl-block nil
      (let ((tail list))
        (while (cdr tail)
          (when (eq item
                    (cadr tail))
            (pop (cdr tail))
            (cl-return))
          (pop tail))))))
  list)
#+end_example

*** TEST-PASSED item place count non-nil, non-zero
#+begin_src elisp :tangle no :results code :wrap example elisp :keep-expected t
(macroexpand-1 `(delqf item place :count n))
#+end_src

#+EXPECTED:
#+begin_example elisp
(let ((item item)
      (list place)
      (left-to-delq n))
  (bugz--check-delqf-count left-to-delq)
  (cond
   ((or (null list)
        (zerop left-to-delq)))
   (t
    (when (eq item
              (car list))
      (pop list)
      (cl-decf left-to-delq))
    (unless (zerop left-to-delq)
      (cl-block nil
        (let ((tail list))
          (while (cdr tail)
            (when (eq item
                      (cadr tail))
              (pop (cdr tail))
              (cl-decf left-to-delq)
              (when (zerop left-to-delq)
                (cl-return)))
            (pop tail)))))))
  list)
#+end_example

* pushid-sorted
** Dependencies
#+begin_src elisp :results none
(eval-when-compile (require 'akater-misc-macs))
#+end_src

** Examples
*** Basic Examples
**** TEST-PASSED Push a new element into a sorted list
#+begin_src elisp :tangle no :results code :wrap example elisp
(let ((list (list 1 2 4)))
  (pushid-sorted 3 list)
  list)
#+end_src

#+EXPECTED:
#+begin_example elisp
(1 2 3 4)
#+end_example

**** TEST-PASSED Strict vs non-strict ordering does not matter for now
#+begin_src elisp :tangle no :results code :wrap example elisp
(cl-values (let ((list (list 1 2 3 4)))
             (pushid-sorted 3.0 list :ordering #'<)
             list)
           (let ((list (list 1 2 3 4)))
             (pushid-sorted 3.0 list :ordering #'<=)
             list))
#+end_src

#+EXPECTED:
#+begin_example elisp
((1 2 3 3.0 4)
 (1 2 3 3.0 4))
#+end_example

**** TEST-PASSED Default test is #'=
Usually, default test is #'eql but we use #'= because we'd rater have a sane default for ordering, and default test should be compatible with it.
#+begin_src elisp :tangle no :results error :wrap example elisp
(let ((list (list 1 2 3 4)))
  (pushid-sorted 3.0 list :when-non-unique (error "Non-unique"))
  list)
#+end_src

#+EXPECTED:
#+begin_error elisp
(error "Non-unique")
#+end_error

*** Applications
**** TEST-PASSED Push a new bug into alist of bugs
#+begin_src elisp :tangle no :results macroexp :wrap example elisp
(pushid-sorted bug (alist-get tracker bugz-reported-by-me
                              nil nil #'bugz-bug-tracker-equal)
  :ordering #'bugz--bugid-greater :ordering-key #'bugz-bugid
  :test #'bugz--bugid-equal
  ;; :test #'bug-equal :key #'identity
  :when-non-unique
  (error "A duplicate bugz-bug %s is initialized; original is %s, found in `%s'"
         bug pushid-oldelt 'bugz-reported-by-me))
#+end_src

#+EXPECTED:
#+begin_example elisp
(let* ((new-0 newelt)
       (list-0 place)
       (ordering-0 #'bugz--bugid-greater)
       (ordering-key-0 #'bugz-bugid)
       (key-0 ordering-key-0)
       (test-0 #'bugz--bugid-equal))
  (cl-check-type list-0 list)
  (let (pushid-prev-cons)
    (declare (type list pushid-prev-cons))
    (if (null list-0) (setf (alist-get tracker bugz-reported-by-me
                                       nil nil #'bugz-bug-tracker-equal)
                            (list new-0))
      (let (pushid-oldelt)
        (do-sublists (pushid-cons list-0 (error
                                          "An error in `pushid-sorted'"))
          (setq pushid-oldelt (car pushid-cons))
          (cond
           ((funcall test-0
                     (funcall key-0 pushid-oldelt)
                     (funcall key-0 new-0))
            (let ((bug new-0)
                  (pushid-sorted-list list-0))
              (error "A duplicate bugz-bug %s is initialized; original is %s, found in `%s'"
                     bug pushid-oldelt 'bugz-reported-by-me)))
           ((funcall ordering-0
                     (funcall ordering-key-0 pushid-oldelt)
                     (funcall ordering-key-0 new-0))
            (when (null (cdr pushid-cons))
              (cl-return (push new-0 (cdr pushid-cons)))))
           (t
            (cl-return (push new-0 (cdr pushid-prev-cons)))))
          (setq pushid-prev-cons pushid-cons))))))
#+end_example

** Definition
#+begin_src elisp :results none
(defmacro-mmxx pushid-sorted ( newelt place &body options
                               &key (ordering `#'<) (ordering-key `#'identity)
                               (test `#'=) (key ordering-key)
                               (when-non-unique
                                ;; should probably be in &tag
                                )
                               &gensym (new newelt) (list place)
                               ordering ordering-key test key)
  "Push NEWELT to PLACE, presuming NEWELT is unique (w.r.t. TEST) there.

Return the fresh cons.

The value of PLACE must be a list.

In WHEN-NON-UNIQUE, the following variables are captured
- PUSHID-NEWELT (a variable is used if newelt is supplied as variable): the new element
- PUSHID-SORTED-LIST: the value of place
- PUSHID-CONS: the cons which car holds the non-unique element
- PUSHID-PREV-CONS: the cons which cadr holds the non-unique element, or nil if the non-unique element is found in the first car of pushid-sorted-list"
  (ignore options)
  `(progn
     (cl-check-type ,list list)
     (let (pushid-prev-cons)
       (declare (type list pushid-prev-cons))
       (if (null ,list) (setf ,place (list ,new))
         (let ((pushid-oldelt (car ,list)))
           ;; we should support this let spec in do-sublists
           ;; if we want to be able
           ;; to declare a very specific type of pushid-oldelt.
           (cl-block nil
             (cond
              ((funcall ,test
                        ;; what is the correct order?
                        (funcall ,key pushid-oldelt)
                        (funcall ,key ,new)
                        )
               (let ((,(if (mmxx-variablep newelt) newelt 'pushid-newelt)
                      ,new)
                     (pushid-sorted-list ,list))
                 ,when-non-unique))
              ;; If we required non-strict predicate for ORDERING,
              ;; maybe we could defer test check in many cases,
              ;; moving it after the following ordering check
              ;; which is why i write that check as a cond clause,
              ;; other than writing (not it)
              ;; and specifying (null (cdr pushid-cons)) as another cond clause
              ((funcall ,ordering
                        (funcall ,ordering-key pushid-oldelt)
                        (funcall ,ordering-key ,new))
               (when (null (cdr ,list))
                 (cl-return (push ,new (cdr ,list)))))
              (t
               (cl-return (push ,new ,place))))
             (do-sublists (pushid-cons ,list (error "An error in `pushid-sorted'"))
               ;; todo: should we protect pushid-cons here?
               (setq pushid-oldelt (car pushid-cons))
               (cond
                ((funcall ,test
                          ;; what is the correct order?
                          (funcall ,key pushid-oldelt)
                          (funcall ,key ,new)
                          )
                 (let ((,(if (mmxx-variablep newelt) newelt 'pushid-newelt)
                        ,new)
                       (pushid-sorted-list ,list))
                   ,when-non-unique))
                ;; If we required non-strict predicate for ORDERING,
                ;; maybe we could defer test check in many cases,
                ;; moving it after the following ordering check
                ;; which is why i write that check as a cond clause,
                ;; other than writing (not it)
                ;; and specifying (null (cdr pushid-cons)) as another cond clause
                ((funcall ,ordering
                          (funcall ,ordering-key pushid-oldelt)
                          (funcall ,ordering-key ,new))
                 (when (null (cdr pushid-cons))
                   (cl-return (push ,new (cdr pushid-cons)))))
                (t
                 (cl-return (push ,new (cdr pushid-prev-cons)))))
               (setq pushid-prev-cons pushid-cons))))))))
#+end_src

** Tests
*** TEST-PASSED Push to the top of a sorted singleton list
#+begin_src elisp :tangle no :results code :wrap example elisp
(let ((list (list 1)))
  (pushid-sorted 2 list :ordering #'>)
  list)
#+end_src

#+EXPECTED:
#+begin_example elisp
(2 1)
#+end_example

* Buffers
** with-file-buffer
#+begin_src elisp :results none
(defmacro bugz--with-file-buffer (filename &rest body)
  "Visit FILENAME, evaluate BODY forms, kill the buffer if there wasn't one.

Visit FILENAME, set the buffer as current, evaluate BODY forms.
Kill the buffer if it did not exist initially."
  (declare (indent 1))
  (let ((filename-o-o (gensym "filename-"))
        (exists-g (gensym "exists-"))
        (buffer-g (gensym "buffer-")))
    `(let* ((,filename-o-o ,filename)
            (,exists-g (get-file-buffer ,filename-o-o))
            (,buffer-g (or ,exists-g (find-file-noselect ,filename-o-o))))
       (unwind-protect (with-current-buffer ,buffer-g (cl-locally ,@body))
         (unless ,exists-g (kill-buffer ,buffer-g))))))
#+end_src

** with-file-buffer-read-only
*** TODO Maybe turn off read-only if the buffer existed but read-only wasn't enabled initially
- State "TODO"       from              [2022-12-04 Sun 22:08] \\
  However, in this particular elisp package, it might actually be useful to enforce read-only, in each case.
*** Definition
#+begin_src elisp :results none
(defmacro bugz--with-file-buffer-read-only (filename &rest body)
  "Like `bugz--with-file-buffer' but in read-only mode.

Visit FILENAME in read-only mode, set the buffer as current,
evaluate BODY forms.  Kill the buffer if it did not exist
initially."
  (declare (indent 1))
  `(bugz--with-file-buffer ,filename
     (read-only-mode)
     ,@body))
#+end_src

* ifmt
#+begin_src elisp :results none
(defsubst ifmt (s &rest objects) (intern (apply #'format s objects)))
#+end_src
